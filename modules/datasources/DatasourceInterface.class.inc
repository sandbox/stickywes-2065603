<?php

interface DatasourceInterface {

  public function get_value_from_argument($argument);

  public function get_derived_value($argument, $value);

  public function get_input_elements($data);

  public function render_field_item($item);
}