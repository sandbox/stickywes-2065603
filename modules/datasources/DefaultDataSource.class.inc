<?php

class DefaultDataSource implements DatasourceInterface {

  public function get_value_from_argument($argument = "defaultValue") {
    return $argument;
  }

  public function get_input_elements($data = NULL) {
    return array(
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#size' => 60,
      '#default_value' => 'Default'
    );
  }

  public function render_field_item($item) {
    return array('#markup' => check_plain($item['value']));
  }

  public function get_derived_value($arg, $value) {
    return FALSE;
  }
}

class APIDataSourceExample implements DatasourceInterface {

  private $url = 'https://api.twitter.com/1.1/';
  private $endpoint = 'search/tweets.json';
  private $datakey = 'errors';

  protected function get_data_values($key = NULL) {
    $url = $this->url . $this->endpoint;
    $request = drupal_http_request($url);
    $data = json_decode($request->data);
    $data = $data->{$this->datakey}[0];
    if (isset($key) && isset($data->{$key})) {
      return $data->{$key};
    }
    else {
      return $data;
    }
  }

  public function get_input_elements($data = NULL) {
    $options = array(
      'code' => 'code',
      'message' => 'message',
    );
    return array(
      '#type' => 'select',
      '#options' => $options
    );
  }

  public function get_value_from_argument($argument = "defaultValue") {
    $val = $this->get_data_values($argument);
    return $val;
  }

  public function get_derived_value($arg, $value) {
    return FALSE;
  }

  public function render_field_item($item) {
    return array('#markup' => check_plain($item['value']));
  }
}
